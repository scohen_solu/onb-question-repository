(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Hayden Wagner
(Course Site): Code School
(Course Name): JavaScript Best Practices
(Course URL): https://www.codeschool.com/courses/javascript-best-practices
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): If [code]hasVehicle = false[/code] then what will [code]console.log(“I am using a “ + hasVehicle ? “car” : “bike”);[/code] log to the console? 
(A): “bike”
(B): “I am using a bike”
(C): “I am using a car”
(D): “car”
(E): “I am using a false”
(Correct): D
(Points): 1
(CF): It will just log ‘car’ because the plus sign concatenates the existing string with the string value of the variable hasVehicle(false). This creates one string that the ternary operator evaluates as true, which then returns the first expression (‘car’). You should wrap the ternary operator condition and expressions in parenthesis to avoid results like this. 
(WF): It will just log ‘car’ because the plus sign concatenates the existing string with the string value of the variable hasVehicle(false). This creates one string that the ternary operator evaluates as true, which then returns the first expression (‘car’). You should wrap the ternary operator condition and expressions in parenthesis to avoid results like this. 
(STARTIGNORE)
(Hint): Make note of the string and the “+” operator in this statement.
(Subject): Ternary Conditionals
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Choose the expressions that would check if a property exists on an object and assign an empty array to that property if it doesn’t exist.(using only native JavaScript, i.e. no user defined functions)(choose 2)
(A): obj.prop = obj.prop ? obj.prop : [ ];
(B): obj.prop = isUndefined ? [ ];
(C): obj.prop = isEmpty([ ]); 
(D): obj.prop = obj.prop || [ ];
(E): obj.prop = obj.prop // [ ];
(Correct): A, D
(Points): 1
(CF): Assignment expressions using a ternary operator or a logical OR operator make assignments based on logical conditions. The correct expression with the OR operator is preferred because it is more concise.
(WF): Assignment expressions using a ternary operator or a logical OR operator make assignments based on logical conditions. The correct expression with the OR operator is preferred because it is more concise.
(STARTIGNORE)
(Hint):
(Subject): Assignment with Ternary and Logical Operators
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Choose the valid examples of “short-circuiting” in the following assignments with logical operators.(choose 3)
(A): var result = “false” && “red”;
(B): var result = false && [1,2,3]; 
(C): var result = undefined && 0;
(D): var result = “blue” || “cat”;
(E): var result = false || 0;
(Correct): B, C, D
(Points): 1
(CF): “Short circuiting” occurs when the first statement is accepted as the value of an expression using logical operators and the second statement is not even evaluated. This occurs if the first statement after an AND operator is false, or if the first statement after an OR operator is true. 
(WF): “Short circuiting” occurs when the first statement is accepted as the value of an expression using logical operators and the second statement is not even evaluated. This occurs if the first statement after an AND operator is false, or if the first statement after an OR operator is true. 
(STARTIGNORE)
(Hint): Consider the differences between the OR (||) operator and the AND(&&) operator.
(Subject): Assignments with Logical Operators
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): When both the left and the right values are true in an assignment with an “AND”(&&) logical operator the right value will be returned as the assignment value, for example:
[code]
var left = true,
    right = true;
var value = left && right;
[/code]
(A): True
(B): False
(Correct): A
(Points): 1
(CF): When all values are truthy the “AND” operator will return the second (rightmost) value.
(WF): When all values are truthy the “AND” operator will return the second (rightmost) value.
(STARTIGNORE)
(Hint):
(Subject): Assignments with Logical Operators
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What does a switch keyword allow us to do?
(A): The switch keyword allows for quick switching of object properties 
(B): The switch keyword allows us run statements based on a value itself without a Boolean check
(C): It allows us to switch to different scopes within our available linked JavaScript files
(D): It defines a block of code in which the local scope is set to an object or object property.
(Correct): B
(Points): 1
(CF): The switch keyword allows us to specify different statements or groups of statements that run based on a provided value and the defined cases in the switch block. The “with” keyword specifies a block where the local scope is set to a passed object.
(WF): The switch keyword allows us to specify different statements or groups of statements that run based on a provided value and the defined cases in the switch block. The “with” keyword specifies a block where the local scope is set to a passed object.
(STARTIGNORE)
(Hint):
(Subject): Switch Blocks
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the term for the process in a switch block when the code block is run from the case that meets the criteria and then the following case code blocks are executed regardless of whether their case criteria was met?
(A): Declaration collapsing
(B): Switch jumping
(C): Zip lining
(D): Step over
(E): Fall through
(Correct): E
(Points): 1
(CF): “Fall through” occurs when break keywords are not used to exit a switch block and the case code blocks following a matched case are executed.
(WF): “Fall through” occurs when break keywords are not used to exit a switch block and the case code blocks following a matched case are executed.
(STARTIGNORE)
(Hint):
(Subject): Switch Blocks
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Caching object properties in a variable is preferred in loops instead of repeatedly accessing an object property.
(A): True 
(B): False
(Correct): A
(Points): 1
(CF): Memory access, especially access to nested properties of objects, is not ideal for normal for-loops. Using a variable to cache a reference to a property value is a good way to solve this performance problem.
(WF): Memory access, especially access to nested properties of objects, is not ideal for normal for-loops. Using a variable to cache a reference to a property value is a good way to solve this performance problem.
(STARTIGNORE)
(Hint):
(Subject): Loop Optimization
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): Choose the correct tips that can improve JavaScript code performance.(choose 3)
(A): Add common object functionality to the object’s prototype
(B): Always append new elements to the DOM individually
(C): Use as few “var” keywords as possible
(D): Use the “join()” method if you are joining strings stored in an array
(E): Use a new “var” keyword for each declaration to save the parser time that it would take to add each one
(Correct): A, C, D
(Points): 1
(CF): Adding common functions to an object’s prototype improves performance because all object instances reference the prototype function instead of declaring and storing their own function. Appending elements to the DOM individually can be slow (use a fragment if you have multiple elements to append). Each new “var” keyword adds a look up to the JS parsers so it is preferred to use a comma for multiple variable declarations. The “join()” method can be a faster and more legible way to concatenate strings in an array than the concatenation operator.
(WF): Adding common functions to an object’s prototype improves performance because all object instances reference the prototype function instead of declaring and storing their own function. Appending elements to the DOM individually can be slow (use a fragment if you have multiple elements to append). Each new “var” keyword adds a look up to the JS parsers so it is preferred to use a comma for multiple variable declarations. The “join()” method can be a faster and more legible way to concatenate strings in an array than the concatenation operator.
(STARTIGNORE)
(Hint):
(Subject): Short Performance Tips
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): The expression [code]('\n \n \t' == 0 )[/code] will return _____.
(A): true 
(B): false
(C): undefined
(D): NaN
(Correct): A 
(Points): 1
(CF): The string on the left side of the equality operator has no actual characters in it so it acts like an empty string, which is equal to 0 with the double equal sign equality operator. Use the triple equal sign equality operator to distinguish strings from numbers.
(WF): The string on the left side of the equality operator has no actual characters in it so it acts like an empty string, which is equal to 0 with the double equal sign equality operator. Use the triple equal sign equality operator to distinguish strings from numbers.
(STARTIGNORE)
(Hint):
(Subject): Equality Comparisons
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): The “instanceof” operator only checks if an object is the direct child of another object
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The “instanceof” operator checks the entire inheritance chain, because an object is an instance of all prototypes from which is inherits properties.
(WF): The “instanceof” operator checks the entire inheritance chain, because an object is an instance of all prototypes from which is inherits properties.
(STARTIGNORE)
(Hint):
(Subject): Careful Comparisons
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

