(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel
30 Java Spring Tutorial

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tsetsen erdene Ganbaatar
(Course Site): Code School
(Course Name): JavaScript Best Practices
(Course URL): https://www.codeschool.com/courses/javascript-best-practices
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): When creating a switch statement with fall through in mind, it is best to declare specific cases first and more general cases (cases that you want to apply for everyone) last.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): If break statements are not used, fall through can help you organise your switch block so that, as soon as one case is true, that case and every case below it also gets executed.
(WF): If break statements are not used, fall through can help you organise your switch block so that, as soon as one case is true, that case and every case below it also gets executed.
(STARTIGNORE)
(Hint):
(Subject): Switch
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Your users are complaining about your website performance. The issue is with your script loading and execution order. Which actions can generally speed up performance? (Select all that apply)
(A): Load your scripts asynchronously.
(B): Delay the script loading as long as possible by moving the script tag below DOM elements.
(C): Combine all of your scripts into one master script so that the html only loads one script.
(D): Make sure your scripts are being loaded first, before anything else.
(Correct): A,B
(Points): 1
(CF): Delay your script loading until absolutely necessary so that the user can at least see the page load fast and the script can load while the user is deciding what to do. Asynchronous loading can help mitigate some of the performance lost from loading a required script.
(WF): Delay your script loading until absolutely necessary so that the user can at least see the page load fast and the script can load while the user is deciding what to do. Asynchronous loading can help mitigate some of the performance lost from loading a required script.
(STARTIGNORE)
(Hint):
(Subject): Script Execution
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the result of this expression?
[code]
4 === '4'
[/code]
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The === operator does strict comparison where the types and values have to match.
(WF): The === operator does strict comparison where the types and values have to match.
(STARTIGNORE)
(Hint):
(Subject): Careful Comparisons
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 1
(Random answers): 1
(Question): How can the below code use better practices? (Select all that apply)
[code]
function fun(){
    var b = 2;
    var message = "";
    for (var i = 0; i < comp.getStuff().lookUp(); i++){
        message += comp.getStringFromArray(i, b);
    }
    return message;
}
[code]

(A): Use inline declarations to declare multiple variables using the [code]var[/code] keyword only once.
(B): Cache the the result of [code]comp.getStuff().lookUp()[/code] as a local variable and use that compare the value of i.
(C): Use the String [code].join()[/code] to concatenate the array elements to the message variable in one go.
(D): Convert the for loop to a while loop.
(E): declare the [code]message[/code] variable as a global variable and have fun just modify the global value. Therefore the function doesn't need to return anything, making it more efficient.
(Correct): A,B,C
(Points): 1
(CF): Declaring multiple variables at a time is more efficient, caching the result means you don't have to perform this complicated lookUp each time and the String join method is a lot more efficient than iterating through the array and concatenating the strings one at a time.
(WF): Declaring multiple variables at a time is more efficient, caching the result means you don't have to perform this complicated lookUp each time and the String join method is a lot more efficient than iterating through the array and concatenating the strings one at a time.
(STARTIGNORE)
(Hint):
(Subject): Loop Optimization
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): You need to add several hundred <li> elements to a <ul> DOM element via JavaScript. How is this best done?
(A): In a loop, create each <li> element and append it as a child to the <ul>.
(B): Use a DocumentFragment and append each <li> element to the fragment. At the end of the loop, add the fragment as a child to the <ul>.
(C): Manually type the elements in the html; Javascript should never be used to modify the DOM.
(D): Create one new <li> element with a default value and append it several hundred times to the DOM. Then go through the DOM and change the default values to the correct ones.
(E): Using a module, perform augmentation on the DOM by passing the entire DOM to a module.
(Correct): B
(Points): 1
(CF): A DocumentFragment is a convenient staging area that you can add DOM elements to without touching the actual DOM until the very end where the elements are all added in batch.
(WF): A DocumentFragment is a convenient staging area that you can add DOM elements to without touching the actual DOM until the very end where the elements are all added in batch.
(STARTIGNORE)
(Hint):
(Subject): Performance Optimization
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): This code is attempting to Augment a module called oldMod. What is the result of running this code?
[code]
oldMod = function(old){
    var augmentedValue = 5;
    return {
        augmentedMethod: function(){},
        val: old
    };
}(oldMod);
[/code]
(A): The variable oldMod is set to the function that takes in old as a parameter.
(B): The variable oldMod is set to the augmented module properly.
(C): The variable oldMod is set to an object that contains the module that we want as a property called val.
(D): The code fails to compile due to a missing semicolon.
(E): The augmented module is set properly but the augmentedMethod is not accessible.
(Correct): C
(Points): 1
(CF): Augmenting functions should directly return the module they took in as a parameter after augmenting it. Here, a new object literal is created that contains the module as a property.
(WF): Augmenting functions should directly return the module they took in as a parameter after augmenting it. Here, a new object literal is created that contains the module as a property.
(STARTIGNORE)
(Hint):
(Subject): Augmentation
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Proper use of inheritance can help with memory efficiency.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Proper use of prototypes to maintain shared behaviour means that the objects that inherit from that prototype doesn't need to maintain their own copy.
(WF): Proper use of prototypes to maintain shared behaviour means that the objects that inherit from that prototype doesn't need to maintain their own copy.
(STARTIGNORE)
(Hint):
(Subject): Short Performance Tips
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the best practice for a module to access a global variable?
(A): With an import statement at the top of the module.
(B): Just reference the global variable by name; JavaScript will find it for you.
(C): Add a parameter to the function and pass the global variable into the function call.
(D): Create a global module that nests every other module.
(E): Create a global hashmap, put the variables there and use the hashmap to lookup the variables.
(Correct): C
(Points): 1
(CF): The global variable is passed down by value and now a local variable exists. This decreases lookup time and the module now modify it's local variable without affecting the global one.
(WF): The global variable is passed down by value and now a local variable exists. This decreases lookup time and the module now modify it's local variable without affecting the global one.
(STARTIGNORE)
(Hint):
(Subject): Modules
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): It is bad practice for a module to directly lookup global variables.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Every reference to a global variable would cause a search through the entire chain of scopes all the way up to the global scope. If you have nested modules, this becomes a computationally expensive process.
(WF): Every reference to a global variable would cause a search through the entire chain of scopes all the way up to the global scope. If you have nested modules, this becomes a computationally expensive process.
(STARTIGNORE)
(Hint):
(Subject): Global Imports
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): This code is trying to set up private attributes and methods via closures. What's occurs when this code is run?
[code]
var obj = (function(){
    var hiddenValue = 5;
    var hiddenMethod = function(){};

    return {
        publicValue: 5,
        publicMethod: function(){}
    };
});

[/code]
(A): The code fails to compile due to a missing semicolon.
(B): The variable obj is properly set to the closure returned by the function.
(C): The variable obj is set to the function itself and not the returned object.
(D): The code fails to compile due to incorrect object literal syntax.
(E): The two variables defined with var actually become globally accessible.
(Correct): C
(Points): 1
(CF): The function was never actually called. The closure was never created. There should be an extra pair of parenthesis at near the end.
(WF): The function was never actually called. The closure was never created. There should be an extra pair of parenthesis at near the end.
(STARTIGNORE)
(Hint):
(Subject): Anonymous Closures
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)