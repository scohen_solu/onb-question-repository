(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel
30 Java Spring Tutorial

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brandon Bires-Navel
(Course Site): Code School
(Course Name): JavaScript Road Trip Part 3
(Course URL): https://www.codeschool.com/courses/javascript-road-trip-part-3/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Assigning a function to a variable without giving the function itself a name is called a(n):
(A): Self-Described function
(B): Anonymous Function
(C): Function logic
(D): Variables
(Correct): B
(Points): 1
(CF): The two most common ways to create a function in JavaScript are by using the function declaration or function operator. 
(WF): The two most common ways to create a function in JavaScript are by using the function declaration or function operator. 
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): The [code].map()[/code] function is a prototype of which JavaScript class?
(A): Object
(B): Collection
(C): Array
(D): String
(E): Map
(Correct): C
(Points): 1
(CF): The [code]Array.map()[/code] function creates a new array with the results of calling a provided function on every element in the calling array.
(WF): The [code]Array.map()[/code] function creates a new array with the results of calling a provided function on every element in the calling array.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is returned on the third call of [code]add();[/code] in the following code snippet:
[code]
var add = (function () {
    var counter = 0;
    return function () { return counter += 1; }
})();

add();
add();
add();
[/code]
(A): 1
(B): undefined -- a number isn't returned
(C): 3
(D): 0
(Correct): C
(Points): 1
(CF): JavaScript closures makes it possible for a function to have "private" variables.
(WF): JavaScript closures makes it possible for a function to have "private" variables.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): The following program won't compile because the variable [code]x[/code] is used before it's declared.
[code]
x = 5;
print(x);
var x;
[/code]
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Variable declarations are hoisted to the top of the current scope.
(WF): Variable declarations are hoisted to the top of the current scope.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Given an object [code]person[/code] that has the attributes [code]age[/code] and [code]gender[/code], how would you give the [code]person[/code] a third attribute [code]height[/code]? (Pick two)
(A): person.setHeight(72);
(B): person["height"] = 72;
(C): person(height) = 72;
(D): person.height = 72;
(Correct): B,D
(Points): 1
(CF): In JavaScript if you assign an attribute of an object where the attribute does not exists JavaScript will create one automatically.
(WF): In JavaScript if you assign an attribute of an object where the attribute does not exists JavaScript will create one automatically.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Given an object [code]person[/code] that has the attributes [code]age[/code] and [code]gender[/code], how would you delete the [code]age[/code] attribute from the object? (Pick two)
(A): delete(person["age"]);
(B): person.age = null;
(C): delete person.age;
(D): delete(person.age);
(E): delete person["age"];
(Correct): C,E
(Points): 1
(CF): The delete keyword deletes both the value of the property and the property itself.
(WF): The delete keyword deletes both the value of the property and the property itself.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): [code]var f = function(){};[/code] instantiates f as a Function object.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): When creating functions use [code]function(){}[/code] instead of [code]new Function()[/code].
(WF): When creating functions use [code]function(){}[/code] instead of [code]new Function()[/code].
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Given the array [code]var animals = ['pigs', 'goats', 'sheep'];[/code], what would be the resulting array after calling [code]animals.push('cows');[/code]
(A): Array ["cows", "goats", "sheep"]
(B): Array ["cows", "pigs", "goats", "sheep"]
(C): Array ["pigs", "goats", "sheep", "cows"]
(D): Array ["goats", "sheep", "cows"]
(Correct): C
(Points): 1
(CF): The push() method adds new items to the end of an array, and returns the new length.
(WF): The push() method adds new items to the end of an array, and returns the new length.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): By default every object in JavaScript inherits specific properties and methods from parent
(A): prototypes
(B): namespaces
(C): classes
(D): scripts
(Correct): A
(Points): 1
(CF): Object properties and methods can be shared through generalized objects that have the ability to be cloned and extended. This is known as prototypical inheritance and differs from class inheritance.
(WF): Object properties and methods can be shared through generalized objects that have the ability to be cloned and extended. This is known as prototypical inheritance and differs from class inheritance.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): When calling an attribute on an object it will search for the attribute starting at the top of the prototype chain starting with the attributes in [code]Object[/code].
(A): True
(B): False
(Correct): B
(Points): 1
(CF): JavaScript will start in the current scope and move up the chain when it doesn't find a match in the current scope.
(WF): JavaScript will start in the current scope and move up the chain when it doesn't find a match in the current scope.
(STARTIGNORE)
(Hint):
(Subject): JavaScript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
