(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Lynda
(Course Name): Java EE Essentials: Enterprise Java Beans
(Course URL): http://www.lynda.com/Java-tutorials/Java-EE-Essentials-Enterprise-JavaBeans/170059-2.html
(Discipline): Java
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a standard of Java Beans? 
(A): Private Properties
(B): Getters and Setters
(C): Serializable
(D): Transient instance variables
(Correct): D
(Points): 1
(CF): If a variable is transient, that means it will not survive serialization, which is problematic for preserving the state of a Java Bean.
(WF): If a variable is transient, that means it will not survive serialization, which is problematic for preserving the state of a Java Bean.
(STARTIGNORE)
(Hint): 
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): What are some issues with programming on a server with Java Beans? (Select all that apply)
(A): Latency
(B): Concurrent Access
(C): No threads
(D): Coding takes longer
(Correct): A, B
(Points): 1
(CF): You need to be aware of multiple processes accessing the same Bean, as well as the latency of data changes.
(WF): You need to be aware of multiple processes accessing the same Bean, as well as the latency of data changes.
(STARTIGNORE)
(Hint):
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): True or false - 2 clients can share the same session bean. 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): A bean can be shared by clients, but it will only last as long as its own client.
(WF): A bean can be shared by clients, but it will only last as long as its own client.
(STARTIGNORE)
(Hint): 
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which value is NOT needed in the @TableGenerator annotation?
(A): Name of the generator
(B): Name of the table in generator
(C): Value of the column to be picked
(D): a GUID
(Correct): D
(Points): 1
(CF): A GUID is not included in the TableGenerator annotation 
(WF): A GUID is not included in the TableGenerator annotation 
(STARTIGNORE)
(Hint):
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): Which of the following should be written on a server? (Select all that apply)
(A): Database modification
(B): Database access
(C): Request Authentication
(D): Taking in User input
(Correct): A, B, C
(Points): 1
(CF): Input should be left to the client
(WF): Input should be left to the client
(STARTIGNORE)
(Hint):
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Any two programs can be parallelized.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Some programs depend on each other.
(WF): Some programs depend on each other.
(STARTIGNORE)
(Hint): 
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following exceptions does the create() method throw on the home interface?
(A): Remote Exception
(B): Null Pointer Exception
(C): Naming Exception
(D): Index Out Of Bounds Exception
(Correct): A
(Points): 1
(CF): The create method will throw a Remote Exception.
(WF): The create method will throw a Remote Exception.
(STARTIGNORE)
(Hint): 
(Subject): Java Beans
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following Access Patterns is the fastest?
(A): CRCW (Concurrent Read Concurrent Write)
(B): ERCW (Exclusive Read Concurrent Write)
(C): EREW (Exclusive Read Exclusive Write)
(C): CREW (Concurrent Read Exclusive Write)
(Correct): A
(Points): 1
(CF): Concurrent read, concurrent write is the fastest, but other problems arise, such as handling when 2 processors try to access the same memory location.
(WF): Concurrent read, concurrent write is the fastest, but other problems arise, such as handling when 2 processors try to access the same memory location.
(STARTIGNORE)
(Hint):
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): True or False - @Lock(Read) automatically inserts a Write lock as well.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): It wouldn't make much sense to allow concurrent writes but not concurrent reads.
(WF): It wouldn't make much sense to allow concurrent writes but not concurrent reads.
(STARTIGNORE)
(Hint):
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): Which of the following are true about Entity Beans? (Select all that apply)
(A): They are essentially permanent 
(B): They are uniquely identified by a primary key
(C): They are associated with a session
(D): There is only one at a time
(Correct): A, B
(Points): 1
(CF): There can be many Entity Beans, and they are associated with a data set, not a session.  This makes them essentially permanent. 
(WF): There can be many Entity Beans, and they are associated with a data set, not a session.  This makes them essentially permanent. 
(STARTIGNORE)
(Hint): 
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

