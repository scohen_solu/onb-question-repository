(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): Lynda
(Course Name): Java EE Essentials: Enterprise Java Beans
(Course URL): http://www.lynda.com/Java-tutorials/Java-EE-Essentials-Enterprise-JavaBeans/170059-2.html
(Discipline): Java
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): Which of the following are required for creating an EJB(Select all that apply)? 
(A): Private Properties
(B): Getters and Setters
(C): Serializable
(D): No arg constructor
(E): toString method implemented
(Correct): A,B,C,D
(Points): 1
(CF): A valid ejb requires the following: private properties, getters/setters, no arg constructor, and must implement serializable.
(WF): A valid ejb requires the following: private properties, getters/setters, no arg constructor, and must implement serializable.
(STARTIGNORE)
(Hint): 
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 1
(Random answers): 1
(Question): What is required to define a primary key in an Entity bean?(Select all that apply)?
(A): Implement comparable
(B): getId method implemented
(C): @TableGenerator annotation
(D): @ID annotation
(E): @ColumnName annotation
(Correct): B,C,D,E
(Points): 1
(CF): The following items are required for defining a primary key: @ID, @TableGenerator, @ColumnName and getId method must be implemented.
(WF): The following items are required for defining a primary key: @ID, @TableGenerator, @ColumnName and getId method must be implemented.
(STARTIGNORE)
(Hint):
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): true/false
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): EREW memory access is faster than CREW memory access?
(A): True
(B): False
(Correct): B
(Points): 1
(CF): EREW is slower because it will not allow any concurrent reads while any other reads/writes are in process.
(WF): EREW is slower because it will not allow any concurrent reads while any other reads/writes are in process.
(STARTIGNORE)
(Hint): 
(Subject): Java Beans
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What annotation is used to define the roles for a bean?
(A): @Role
(B): @IncludedRoles
(C): @DeclareRoles
(D): None of the above
(Correct): C
(Points): 1
(CF): You can define the roles for your bean with the @DeclareRoles annotation.
(WF): You can define the roles for your bean with the @DeclareRoles annotation.
(STARTIGNORE)
(Hint):
(Subject): Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which type of bean is commonly used for performing complex tasks?
(A): Message bean
(B): Entity bean
(C): Session bean
(D): Java bean
(Correct): B
(Points): 1
(CF): Entity beans are commonly used to perform more complex tasks.
(WF): Entity beans are commonly used to perform more complex tasks.
(STARTIGNORE)
(Hint):
(Subject): Java Beans
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
