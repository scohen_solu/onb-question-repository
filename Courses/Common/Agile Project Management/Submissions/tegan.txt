(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Terry Egan
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following definitions of velocity is correct?
(A): Velocity measures the speed required to finish a project.
(B): Velocity measures the speed it takes to complete one feature.
(C): Velocity measures the number of functions the team is completing in the average sprint. Helps you determine and understand what your production rate is.
(D): Velocity measures the number of features that you complete in the product backlog.
(E): Velocity measures the number of bugs created in a sprint.
(Correct): C 
(Points): 1
(CF): Velocity is defined as how many functions the team is completing in the average sprint. Helps you determine and understand what your production rate is.
(WF): Velocity is defined as how many functions the team is completing in the average sprint. Helps you determine and understand what your production rate is.
(STARTIGNORE)
(Hint):
(Subject): Definitions
(Difficulty): Intermediate 
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not true about the Agile Lifecycle?
(A): The primary purpose of the Speculate phase is for the business and technical teams to identify the features for this iteration.
(B): The Adapt phase immediately follows the Explore Phase.
(C): At the end of the Envision stage, you should have a documented project charter.
(D): The Speculate phase is only performed once during a project.
(E): Stand-up Meetings are a component of the Explore phase.
(Correct): D
(Points): 1
(CF): Only the Envision and Close phase are completed only once.
(WF): Only the Envision and Close phase are completed only once.
(STARTIGNORE)
(Hint):
(Subject): Agile Lifecycle
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which statement is false about an Agile Project?
(A): Since a Sprint is time-bound, a Sprint has a limit of one feature.
(B): A story is a short description of functionality described from the perspective of a user or customer.
(C): Agile projects deliver value to the business in frequent small deliveries of product called features.
(D): Agile looks at the project as a collection of chunks.  Those chunks are divided by sprints and features.
(E): Values and Norms are a set of rules and guidelines for the Scrum Team to follow.
(Correct): A
(Points): 1
(CF): A Sprint is defined as a releasable iteration, specifically a collection of features.
(WF): A Sprint is defined as a releasable iteration, specifically a collection of features.
(STARTIGNORE)
(Hint):
(Subject): Scrum Artifacts
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which choice provides the best definition of Snowplowing?
(A): The practice of eliminating the testing phase in order to finish the project on time.
(B): Unimplemented features being added to the backlog list at the end of a sprint.
(C): Termination of an Agile project.
(D): Adding additional resources to a project.
(E): The creation of a new Sprint.
(Correct): B
(Points): 1
(CF): The definition of snowplowing is an unimplemented feature being added to the backlog list at the end of a sprint.
(WF): The definition of snowplowing is an unimplemented feature being added to the backlog list at the end of a sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Artifacts
(Difficulty): Intermediate 
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which item is the least important element of an Agile project?
(A): Formal Documentation
(B): Sprint
(C): Product Backlog
(D): User Stories
(E): Feature
(Correct): A
(Points): 1
(CF): Stakeholders are more interested in the product than the documentation.  Although, some documentation such as a Project Charter is required, the actual product provides real business value.
(WF): Stakeholders are more interested in the product than the documentation.  Although, some documentation such as a Project Charter is required, the actual product provides real business value.
(STARTIGNORE)
(Hint):
(Subject): Scrum Artifacts
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which role is the least accurate regarding the Scrum Team?
(A): The Scrum Team is the car itself, ready to speed along in whatever direction it is pointed.
(B): The Product Owner is the driver, making sure that the car is always going in the right direction.
(C): The Scrum Master is responsible for delegating tasks to the team.
(D): The Scrum team, despite their title, is responsible for ensuring the Sprint deadlines are met.
(E): The Scrum Master is the chief mechanic, keeping the car well-tuned and performing at its best.
(Correct): C
(Points): 1
(CF): The Scrum Master's role is not a delegator.
(WF): The Scrum Master's role is not a delegator.
(STARTIGNORE)
(Hint):
(Subject): Scrum Team
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following components is true for an Agile project?
(A): All of the features of a product must be delivered in the same release.
(B): Writing documentation is a key component.
(C): The prioritization of features cannot be modified.
(D): Requirements can and are expected to change at any time during the project.
(E): Subject Matter Experts and users do not need to be involved in an Agile project.
(Correct): D
(Points): 1
(CF): One of the key fundamentals and benefits of an Agile Project is the requirements can and will change at any time as more information is discovered during the course of the project.
(WF): One of the key fundamentals and benefits of an Agile Project is the requirements can and will change at any time as more information is discovered during the course of the project.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following statements are not true of the Close phase in an Agile project?
(A): It includes closing activities such as paperwork and communications.
(B): The close is the last Sprint of a project.
(C): It includes ensuring vendors are paid and payments are received.
(D): It includes communicating the overall project results.
(E): It includes updating risk register.
(Correct): E
(Points): 1
(CF): Updating the risk register is actually part of the Sprint Meeting.
(WF): Updating the risk register is actually part of the Sprint Meeting.
(STARTIGNORE)
(Hint): 
(Subject): Agile Lifecycle
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which statement best describes the value of a Sprint?
(A): A Sprint is a small collection of features, so the Scrum Team can provide more accurate estimates and track the amount of work to be done more accurately for a project chunk.
(B): A Sprint must be formally written in a requirements document.
(C): Sprints are useful for helping to determine the total amount of effort required complete an entire project.
(D): Sprints are a formal document designed to list all of the requirements for an Agile project.
(E): A Sprint is designed to ensure the Scrum Team works at a faster pace.
(Correct): A
(Points): 1
(CF): Sprints are easier to estimate, because they contain a relatively small number of features, which make them easier to estimate the duration.  Also, since Sprints are releasable, the customer can gain business value sooner.
(WF): Sprints are easier to estimate, because they contain a relatively small number of features, which make them easier to estimate the duration.  Also, since Sprints are releasable, the customer can gain business value sooner.
(STARTIGNORE)
(Hint):
(Subject): Scrum Artifacts
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which statement is not true about the role of the Product Owner?
(A): The Product Owner creates a compelling vision of the product, and then conveying that vision to the team through the product backlog.
(B): The Product Owner is responsible for prioritizing the backlog during the Scrum development.
(C): Once the Agile project has completed the Speculate phase, they do not need to participate in the project until the beginning of the Close phase.
(D): The Product Owner acts as the navigator for the project.
(E): 
(Correct): C
(Points): 1
(CF): The Product Owner is active during the whole Agile Lifecycle.  For example, the role of the Product Owner is to continually monitor and reprioritize the Product Backlog.  This activity is a big part of the Speculate/Explore/Adapt phases.
(WF): The Product Owner is active during the whole Agile Lifecycle.  For example, the role of the Product Owner is to continually monitor and reprioritize the Product Backlog.  This activity is a big part of the Speculate/Explore/Adapt phases.
(STARTIGNORE)
(Hint):
(Subject): Scrum Team
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


