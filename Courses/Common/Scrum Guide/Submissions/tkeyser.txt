(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):
(Course Site):
(Course Name):
(Course URL):
(Discipline):
(ENDIGNORE)

(Type): truefale
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum is lightweight, simple to understand, and easy to master.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Scrum is lightweight, simple to understand, and difficult to master.
(WF): Scrum is lightweight, simple to understand, and difficult to master.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum has been used since the early ____.
(A): 1990s
(B): 2000s
(C): 1980s
(D): 1970s
(Correct): A
(Points): 1
(CF): Scrum is a process framework that has been used to manage complex product development
since the early 1990s. 
(WF): Scrum is a process framework that has been used to manage complex product development
since the early 1990s. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The rules of Scrum bind together the events, roles, and artifacts, governing the relationships and
interaction between them. 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): The rules of Scrum bind together the events, roles, and artifacts, governing the relationships and
interaction between them. 
(WF): The rules of Scrum bind together the events, roles, and artifacts, governing the relationships and
interaction between them. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Inspections are most beneficial when diligently performed by who?
(A): Everyone.
(B): The stakeholders.
(C): Skilled inspectors.
(D): New people joining the team that have a fresh perspective.
(Correct): C
(Points): 1
(CF): Inspections are most beneficial when diligently performed by skilled inspectors at
the point of work.
(WF): Inspections are most beneficial when diligently performed by skilled inspectors at
the point of work.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum Teams deliver products iteratively and incrementally, minimizing opportunities for
feedback. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Scrum Teams deliver products iteratively and incrementally, maximizing opportunities for
feedback. 
(WF): Scrum Teams deliver products iteratively and incrementally, maximizing opportunities for
feedback. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The product owner is one person, not a committee.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): The Product Owner is one person, not a committee. The Product Owner may represent the
desires of a committee in the Product Backlog, but those wanting to change a Product Backlog
items priority must address the Product Owner.
(WF): The Product Owner is one person, not a committee. The Product Owner may represent the
desires of a committee in the Product Backlog, but those wanting to change a Product Backlog
items priority must address the Product Owner.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question):  No one (not even the Scrum Master) tells the Development Team
how to turn Product Backlog into Increments of potentially releasable functionality.
(A): True
(B): False
(Correct): A
(Points): 1
(CF):  No one (not even the Scrum Master) tells the Development Team
how to turn Product Backlog into Increments of potentially releasable functionality
(WF):  No one (not even the Scrum Master) tells the Development Team
how to turn Product Backlog into Increments of potentially releasable functionality
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who is the servant-leader for the Scrum team?
(A): Scrum master
(B): Product owner
(C): Management
(D): Stakeholders
(Correct): A
(Points): 1
(CF): The Scrum Master is a servant-leader for the Scrum Team.
(WF): The Scrum Master is a servant-leader for the Scrum Team.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question):  Like projects, Sprints are not used to accomplish something.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF):  Like projects, Sprints are used to accomplish something.
(WF):  Like projects, Sprints are used to accomplish something.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): A Sprint cannot be cancelled before the Sprint time-box is over.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): A Sprint can be cancelled before the Sprint time-box is over.
(WF): A Sprint can be cancelled before the Sprint time-box is over.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)