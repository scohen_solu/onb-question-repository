(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Udemy
(Course Name): RESTful API Testing with Postman
(Course URL): https://www.udemy.com/restful-api-testing-with-postman/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 25
(Grade style): 0
(Random answers): 1
(Question): You are configuring an API call in Postman to use the image_id variable in an API call.  Both an environment variable and a Global variable are provided for image_id.  If no environment is selected for the current API call, which value will be used?
(A): No value
(B): The value of the environment variable
(C): The value of the global variable
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Postman Variables
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 0
(Random answers): 0
(Question): True or False: If the same variable exists in both environment and global section, variable in the environment section gets priority.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Postman Variables
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 2
(Random answers): 0
(Question): Which options below are benefits of using the Collections feature of Postman?
(A): Collections allow you to group related things together.
(B): Collections with tests can all be run at the same time.
(C): Collections can be shared with colleagues.
(D): All of the above
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Postman Collections
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 2
(Random answers): 1
(Question): Which are the items you need to consider when sending an HTTP request in Postman? (Choose 4)
(A): Headers
(B): Method
(C): Body
(D): URL
(E): Collection
(F): Response
(Correct): A,B,C,D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Postman Requests
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 0
(Random answers): 1
(Question): True or False: Postman supports automatic URI Component Encoding and Decoding of selected text.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Postman Configuration
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 0
(Random answers): 1
(Question): When writing a test for Postman you want it to fail if the API responds with a 404 error.  Assuming you have already initialized a variable called responseBody to contain the API response, how would you accomplish this?
(A): responseBody.code !== 200
(B): responseBody.code === 404
(C): responseBody.code !== 404
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Postman Testing
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 0
(Random answers): 1
(Question): What is the name of the companion tool for Postman that allows you to run shared/exported Postman tests from a command line?
(A): Newman
(B): Oldman
(C): Postward
(D): PostmanCLI
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Postman Testing
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 0
(Random answers): 1
(Question): True or False: Newman can be used to integrate tests from Postman with Continuous Integration Servers and Build Servers.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Newman
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 0
(Random answers): 1
(Question): If you use the --bail option when running a test through Newman, what will happen after a test fails?
(A): Tests will continue to be executed until the specified number of iterations has finished
(B): Tests will continue to run until the current iteration has completed, and then stop
(C): Tests will exit immediately
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Newman
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 25
(Grade style): 0
(Random answers): 1
(Question): True or false: Newman allows you to pass in additional variables at run time.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Newman
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)